# -*- coding: utf-8 -*-
# vim: ft=python fileencoding=utf-8 sts=4 sw=4 et:
"""webservice Django app views."""

import asyncio
import base64
import httpx
import logging

from django.conf import settings
from django.http import (
    HttpRequest,
    HttpResponse,
    HttpResponseBadRequest,
    JsonResponse,
)

# from django.views.decorators.csrf import csrf_exempt
from io import BytesIO
from pathlib import Path
from PIL import Image
from typing import Dict, Optional, Union
from uuid import uuid4

from .utils import decode_img

LOGGER = logging.getLogger("webservice.views")


# @csrf_exempt
async def v1hka(request: HttpRequest) -> HttpResponse:
    """Handels GET/POST request for Heizkostenabrechnungen.

    GET/POST parameters:
      pages: List of pages with page name as key and base64 encoded image as value.
      mdl: Messdienstleister (optional)
    """
    params = request.POST.copy() if request.method == "POST" else request.GET.copy()

    mdl = "sonstige"
    if "mdl" in params:
        mdl = params.pop("mdl")[0].lower()
    elif "MDL" in params:
        mdl = params.pop("MDL")[0].lower()
    if mdl not in ["techem", "minol", "kalo", "bfw", "brunata", "ista", "sonstige"]:
        return HttpResponseBadRequest(f'Unkown MDL "{mdl}".')

    pages = {}
    if "pages" in params:
        for i, page_imgs in enumerate(params.pop("pages")):
            for k, v in page_imgs.items():
                pages[k] = v
    elif "PAGES" in params:
        for i, page_imgs in params.pop("PAGES"):
            for k, v in page_imgs.items():
                pages[k] = v
    else:
        return HttpResponseBadRequest('Missing "pages" param.')

    data = {}
    for page_name, img_data in pages.items():
        uuid = str(uuid4())
        try:
            data[uuid] = {"name": page_name, "work_dir": decode_img(uuid, img_data)}
        except Exception as e:
            return HttpResponseBadRequest(
                f"An error occured while decoding image and creating mets.xml. {e}"
            )

    response: Dict[str, Dict[str, Dict[str, str]]] = {"pages": {}}
    for uuid, v in (await _hka(mdl, data)).items():
        response["pages"][v["name"]] = {
            "page": v["page"],
            "img": v["img"],
            "msg": v["msg"],
            "status_code": v["status_code"],
        }

    return JsonResponse(response)


# @csrf_exempt
async def v2hka(request: HttpRequest) -> HttpResponse:
    """Handels GET/POST request for Heizkostenabrechnungen.

    GET/POST parameters:
      page: Base64 encoded image
      mdl: Messdienstleister (optional)
    """
    params = request.POST.copy() if request.method == "POST" else request.GET.copy()

    mdl = "sonstige"
    if "mdl" in params:
        mdl = params.pop("mdl")[0].lower()
    elif "MDL" in params:
        mdl = params.pop("MDL")[0].lower()
    if mdl not in ["techem", "minol", "kalo", "bfw", "brunata", "ista", "sonstige"]:
        return HttpResponseBadRequest(f'Unkown MDL "{mdl}".')

    img_data = None
    if "page" in params:
        img_data = params.pop("page")[0]
    elif "PAGE" in params:
        img_data = params.pop("PAGE")[0]
    else:
        return HttpResponseBadRequest('Missing "page" param.')

    uuid = str(uuid4())
    data = (await _hka(mdl, {uuid: {"work_dir": decode_img(uuid, img_data)}}))[uuid]

    return JsonResponse(
        {
            "page": data["page"],
            "img": data["img"],
            "msg": data["msg"],
            "status_code": data["status_code"],
        }
    )


async def _hka(
    mdl: str, pages: Dict[str, Dict[str, Union[str, Path]]]
) -> Dict[str, Dict[str, Optional[str]]]:
    LOGGER.info(f"Send {len(pages.keys())} request to {mdl}.")
    async with httpx.AsyncClient() as client:
        rs = await asyncio.gather(
            *[
                client.get(
                    f"http://backend_{mdl}:7001/process",
                    params={"mets": f"{v['work_dir']}/mets.xml"},
                    timeout=None,
                )
                for k, v in pages.items()
            ]
        )

    responses = {}
    for (k, v), r in zip(pages.items(), rs):
        LOGGER.info(f"Response for {k}: {r.status_code}, {r.text}.")
        responses[k] = {
            "name": str(v["name"]) if "name" in v else None,
            "page": None,
            "img": None,
            "msg": r.text,
            "status_code": r.status_code,
        }

        if r.status_code == httpx.codes.ok:
            img_path = (
                v["work_dir"]
                / settings.OCRD_IMG
                / f"{settings.OCRD_IMG}_{k}{settings.OCRD_IMG_SUFFIX}.png"
            )
            page_xml_path = (
                v["work_dir"]
                / settings.OCRD_PAGE_XML
                / f"{settings.OCRD_PAGE_XML}_{k}.xml"
            )
            if img_path.exists():
                img = Image.open(img_path)
                img_file = BytesIO()
                img.save(img_file, format="PNG")
                responses[k]["img"] = base64.b64encode(img_file.getvalue()).decode(
                    "utf8"
                )

            if page_xml_path.exists():
                with open(
                    page_xml_path,
                    "r",
                    encoding="utf8",
                ) as f:
                    responses[k]["page"] = f.read()

    return responses
