# Webservice
AI frontend for SmartHEC project: OCR extraction of relevant information from scanned forms via context recognition

## Install

* Install [Docker CE](https://docs.docker.com/install/) and [Docker Compose](https://docs.docker.com/compose/install/).
* For GPU support, also install [Nvidia Runtime Toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html).

### Development
* Local Django config in `local.py`, for details see [**Settings**](#settings).
* Run without Docker:
  * Create virtual environment and activate it and install dependencies:

        virtualenv -p /usr/bin/python3 .venv
        source .venv/bin/activate
        pip install -r requirements

  * Migrate database, only Django base

        python manage.py migrate

  * Run Django dev server

        python manage.py runserver

* Run with Docker:
  * Run and build Docker image:

        docker-compose pull
        docker-compose up --build

### Production
* Local Django config in `local.py`, for details see [**Settings**](#settings).
* Run Docker container:

    docker-compose pull
    docker-compose up --build -d


> TODO: document how to `docker stack deploy` and `docker swarm init/join` ...

## Settings
### Docker-Compose

Customize (or override) settings in `docker-compose.yml`:
- mount points / volumes
- environment variables:
   * `WORKERS`: number of parallel processes on this workflow server
   * `CUDA_WORKERS`: number of GPU workers among `WORKERS` (set 0 to get only CPU workers)
   * `MRCNNPROCS`: number of GPU workers or tasks in total on this device (at least sum of all our `CUDA_WORKERS`)
   * `IMAGES_PER_GPU`: batch size for Tensorflow on GPU workers
   * `MDL`: Messdienstleister model to load on this workflow server

### Django

Local config for Django is in `local.py` can look like:
```python
import logging.config

DEBUG = False # Only in production
TIME_ZONE = 'Europe/Berlin'
ADMINS = [('NAME', 'EMAIL')]
ALLOWED_HOSTS = ('localhost', '127.0.0.1', '0.0.0.0')

# Needs to be set.
OCRD_PAGE_XML = "OCR-D-OCR-TESS-deu-SEG-tesseract-sparse-FORM-OCR"
OCRD_IMG = "OCR-D-IMG-BINPAGE-sauvola-DENOISE-ocropy-DESKEW-tesserocr-CROP"
OCRD_IMG_SUFFIX = ".IMG-CROP"

DATA_UPLOAD_MAX_MEMORY_SIZE = 50 * 1024 * 1024 # 50MB
FILE_UPLOAD_MAX_MEMORY_SIZE = 50 * 1024 * 1024

# Workers available per backend

WORKER_SIZE = 3 # Defaut
WORKER_SLEEP = 0.25 # Defaut
VIEW_SLEEP = 0.25 # Defaut

# Logging Configuration

# Clear prev config
LOGGING_CONFIG = None

# Get loglevel from env
LOGLEVEL = "INFO"

logging.config.dictConfig({
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "console": {
            'format': "{asctime} {levelname} [{name}] {message}",
            'style': '{',
        },
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "console",
        },
    },
    "loggers": {
        "": {
            "level": LOGLEVEL,
            "handlers": ["console"],
        },
    },
})
```

## API
* `/api/v1/hka` Post JSON with base64-encoded images, return JSON with PAGE-XML and base64-encoded transformed images; JSON references each page by (arbitrary) name, same for input and output.
  * add `mdl` key in JSON request to define which trained Mask R-CNN to use, possible values are `techem`, `minol`, `kalo`, `bfw`, `brunata`, `ista`, or `sonstige`/`misc`. If given value is not understood or missing `sonstige` will be used.
  * through the `WORKER_SIZE` config it is defined how many parallel requests per MDL will be made to the backend, minimum should be the same as the number of replicas. All other requests will be cached in Redis.
  * If `page` or `img` is `null` the page XML/image couldn't be found. Either they weren't created or the configuration is wrong.
  * **The docker-compose log states, that for every worker (defined by gunicorn) there are `WORKER_SIZE` parallel requests, which is false. The Redis queue is global, e.g. across gunicorn workers.**
  * Request:
    ```
    curl -H "Content-Type: application/json" -X POST -d @- http://localhost:8000/api/v1/hka <<< "{\"mdl\": \"techem\", \"pages\": {\"page01\": \"$(base64 path/to/image1.jpg)\", \"page02\": \"$(base64 path/to/image2.png)\"}}"
    ```

  * Response:
    ```json
    {
      "pages": {
        "page01": {
          "page": "PAGE-XML",
          "img": "base64-encoded transformed image",
          "msg": "Response message from backend, should be 'Finished'.",
          "status_code": "HTML repsonse code from backend requests, should be 200."
        },
        "page02": {
          "page": "PAGE-XML",
          "img": "base64-encoded transformed image",
          "msg": "Response message from backend, should be 'Finished'.",
          "status_code": "HTML repsonse code from backend requests, should be 200."
        }
      }
    }
    ```

  * query resulting PAGE-XML (`pc="http://schema.primaresearch.org/PAGE/gts/pagecontent/2019-07-15"`) for
    - text of a _context_ annotation for class `gebaeude_heizkosten_raumwaerme`:
      ```xpath
      //*[contains(@custom,"subtype:context=gebaeude_heizkosten_raumwaerme")]/pc:TextEquiv[1]/pc:Unicode/text()
      ```

      (`*` because it could be `pc:TextLine` or `pc:Word`)
    - respective polygon outline (as white-space separated points, each a comma-separated x/y pair):
      ```xpath
      //*[contains(@custom,"subtype:context=gebaeude_heizkosten_raumwaerme")]/pc:Coords/@points
      ```

      (all coordinates relate to image under `/pc:PcGts/pc:Page/@imageFilename`)
    - text of a _target_ annotation for class `gebaeude_heizkosten_raumwaerme`:
      ```xpath
      //pc:TextLine[contains(@custom,"subtype:target=gebaeude_heizkosten_raumwaerme")]/pc:TextEquiv[1]/pc:Unicode/text()
      ```
    - respective polygon outline:
      ```xpath
      //pc:TextLine[contains(@custom,"subtype:target=gebaeude_heizkosten_raumwaerme")]/pc:Coords/@points
      ```
    - Coordination transformation matrix (in numpy notation):
      ```xpath
      /PcGts/Page/@custom="coords=[...]"
      ```
* `/api/v2/hka` Post JSON with base64-encoded images, return JSON with PAGE-XML and base64-encoded transformed images, see also `api/v1/hka`.
  * add `mdl` key in JSON request to define which trained Mask R-CNN to use, possible values are `techem`, `minol`, `kalo`, `bfw`, `brunata`, `ista`, or `sonstige`/`misc`. If given value is not understood or missing `sonstige` will be used.
  * Request:
    ```
    curl -H "Content-Type: application/json" -X POST -d @- http://localhost:8000/api/v1/hka <<< "{\"mdl\": \"techem\", \"page\": \"$(base64 path/to/image1.jpg)\"}"
    ```
  * Response:
    ```json
    {
      "page": "PAGE-XML",
      "img": "base64-encoded transformed image",
      "msg": "Response message from backend, should be 'Finished'.",
      "status_code": "HTML repsonse code from backend requests, should be 200."
    }
    ```

## Swarm

1. Setup Docker Swarm
  1. Setup manager: [`docker swarm init --advertise-addr IP`](https://docs.docker.com/engine/swarm/swarm-tutorial/create-swarm/).
  2. Add nodes to swarm: [`docker swarm join-token manager`](https://docs.docker.com/engine/swarm/swarm-tutorial/add-nodes/).
2. Setup Docker registry: [`docker service create --name registry --publish published=5000,target=5000 registry:2`](https://docs.docker.com/engine/swarm/stack-deploy/#set-up-a-docker-registry)
3. Push to registry: `docker-compose push`, might need to run `docker-compuse up -d` and `docker-compose down --volumnes` first.
4. Run with stack deploy: [`docker stack deploy --compose-file docker-compose.yml smarthec`](https://docs.docker.com/engine/swarm/stack-deploy/#deploy-the-stack-to-the-swarm).


Bring down stack with: `docker stack rm smarthec`.
