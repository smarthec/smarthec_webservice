"""Webservice Django app utils."""
import base64
import shutil

from django.conf import settings
from io import BytesIO
from pathlib import Path
from PIL import Image


METS_TEMPLATE = (
    '<?xml version="1.0" encoding="UTF-8"?>\n<mets:mets xmlns:mets="'
    + 'http://www.loc.gov/METS/" xmlns:xlink="http://www.w3.org/1999/xlink" '
    + 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="'
    + "info:lc/xmlns/premis-v2 http://www.loc.gov/standards/premis/v2/premis-v2-0.xsd"
    + " http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-6.xsd "
    + "http://www.loc.gov/METS/ http://www.loc.gov/standards/mets/version17/"
    + "mets.v1-7.xsd http://www.loc.gov/mix/v10 http://www.loc.gov/standards/mix/"
    + 'mix10/mix10.xsd">\n  <mets:metsHdr CREATEDATE="2021-01-18T09:51:58.057643">\n'
    + '    <mets:agent TYPE="OTHER" OTHERTYPE="SOFTWARE" ROLE="CREATOR">\n      '
    + "<mets:name>ocrd/core v2.21.0</mets:name>\n    </mets:agent>\n  </mets:metsHdr>"
    + '\n  <mets:dmdSec ID="DMDLOG_0001">\n    <mets:mdWrap MDTYPE="MODS">\n      '
    + '<mets:xmlData>\n        <mods:mods xmlns:mods="http://www.loc.gov/mods/v3">'
    + "</mods:mods>\n      </mets:xmlData>\n    </mets:mdWrap>\n  </mets:dmdSec>\n  "
    + '<mets:amdSec ID="AMD">\n    </mets:amdSec>\n  <mets:fileSec>\n    '
    + '<mets:fileGrp USE="OCR-D-IMG">\n%(METS_FILES)s    </mets:fileGrp>\n  '
    + '</mets:fileSec>\n  <mets:structMap TYPE="PHYSICAL">\n    <mets:div '
    + 'TYPE="physSequence">\n%(METS_DIVS)s    </mets:div>\n  </mets:structMap>\n'
    + "</mets:mets>"
)


METS_FILE_TEMPLATE = (
    '      <mets:file MIMETYPE="image/png" ID="OCR-D-IMG_%(ID)s">'
    + '\n        <mets:FLocat LOCTYPE="OTHER" OTHERLOCTYPE="FILE" '
    + 'xlink:href="%(PATH)s"/>\n      </mets:file>\n'
)

METS_DIV_TEMPLATE = (
    '      <mets:div TYPE="page" ID="%(ID)s">\n        '
    + '<mets:fptr FILEID="OCR-D-IMG_%(ID)s"/>\n      </mets:div>\n'
)


def decode_img(uuid: str, img_data: str) -> Path:
    """Decode base64 image and save it to harddrive and create mets file for it."""
    work_dir = settings.MEDIA_ROOT / uuid
    work_dir.mkdir()

    try:
        img_path = work_dir / f"{uuid}.png"
        img = Image.open(BytesIO(base64.b64decode(img_data)))
        if img.mode != "RGB":
            img = img.convert("RGB")
        img.save(img_path)

        mets_files = METS_FILE_TEMPLATE % {"ID": uuid, "PATH": img_path}
        mets_divs = METS_DIV_TEMPLATE % {"ID": uuid}
        mets = METS_TEMPLATE % {"METS_FILES": mets_files, "METS_DIVS": mets_divs}
        with open(work_dir / "mets.xml", "w", encoding="utf8") as f:
            f.write(f"{mets}\n")
    except Exception as e:
        if not settings.DEBUG:
            shutil.rmtree(work_dir)
        raise e

    return work_dir
