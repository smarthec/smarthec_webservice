"""Webservice Django app config."""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class WebserviceConfig(AppConfig):
    """Webservice app config."""

    name = "webservice"
    verbose_name = _("Webservice")
    verbose_name_plural = _("Webservices")
