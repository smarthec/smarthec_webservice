"""Django middlewares."""
from django.http import HttpResponseBadRequest, QueryDict

import json


class JSONMiddleware:
    """Process application/json requests data from GET and POST requests.

    Taken from: https://gist.github.com/LucasRoesler/700d281d528ecb7895c0
    """

    def __init__(self, get_response):
        """Init JSONMiddleware."""
        self.get_response = get_response

    def __call__(self, request):
        """Process request data and convert JSON."""
        if request.META.get("CONTENT_TYPE") and "application/json" in request.META.get(
            "CONTENT_TYPE"
        ):
            try:
                data = json.loads(request.body)

                q_data = QueryDict("", mutable=True)
                for key, value in data.items():
                    if isinstance(value, list):
                        for x in value:
                            q_data.update({key: x})
                    else:
                        q_data.update({key: value})

                if request.method == "GET":
                    request.GET = q_data

                if request.method == "POST":
                    request.POST = q_data

                return self.get_response(request)
            except json.JSONDecodeError:
                return HttpResponseBadRequest("JSON Decode Error.")

        return self.get_response(request)
