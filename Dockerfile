# Pull official base image
FROM python:3-alpine

# Set work directory

WORKDIR /usr/src/webservice

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1


# Install dependencies
RUN apk add zlib-dev jpeg-dev gcc musl-dev build-base
RUN pip install --upgrade pip setuptools
COPY ./requirements.txt .
RUN pip install -r requirements.txt

COPY ./entrypoint.prod.sh .

# Copy project
COPY . .

# Run entrypoint.sh
STOPSIGNAL SIGTERM
ENTRYPOINT ["/usr/src/webservice/entrypoint.prod.sh"]
